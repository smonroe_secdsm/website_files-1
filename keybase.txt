==================================================================
https://keybase.io/secdsm_ia
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://secdsm.org
  * I am secdsm_ia (https://keybase.io/secdsm_ia) on keybase.
  * I have a public key with fingerprint DF6C B3C9 6DFF 98FA E2B3  FBEB 25E9 5A9B D848 6AA0

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101161158988873c1b172d8af2e1f0f12a0ed7899e31b3f7b1b65b856ad33d9a18a0a",
      "fingerprint": "df6cb3c96dff98fae2b3fbeb25e95a9bd8486aa0",
      "host": "keybase.io",
      "key_id": "25e95a9bd8486aa0",
      "kid": "0101161158988873c1b172d8af2e1f0f12a0ed7899e31b3f7b1b65b856ad33d9a18a0a",
      "uid": "7b9aa23c4fdb780ef812bbda884e4519",
      "username": "secdsm_ia"
    },
    "service": {
      "hostname": "secdsm.org",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1508697906,
  "expire_in": 157680000,
  "prev": "c9863012470963fcfbe62fc7790152aee32920b4673a7f1c2d7374190bd284b0",
  "seqno": 2,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.76
Comment: https://keybase.io/crypto

yMIjAnicrVJbbA1BGD7VqxN1Jw0Jsi4hmtqZPbs7c0ikhBdpEZciOGZ2Ztvt5ezp
7vbQW6otRSKpB6RBRCIkGh6oW0NKRV1SadRDHwiCUElb2koqRMJsw4PEo3mZ/H++
7/u//8t/Pz0xEEx46Kbk3gi21iZ0th8vC2zt74WVErVZuRSulIr46MeLGXe9SJHF
pLAkAxkADQAVYYSQrhiAAh0yREzIgSmbABKZMx1hzBVAFVOngGoqRapGmKIwTAAi
MpEyJdOK5nMn5lhRT8gyUzOoYmCNmSZGJuFQcCmnUOVYJZgyFEIaIbIgFtiuzxDm
KHF5lmWLnigio/b+gf/PvstG5XSKCYGKETIZ1ZHMTQQgpYwgFOIhFWAf6HInSkq4
QLvcYG5JxCJSdaYonLhlcD9Zf5W/IFm2ky+oMcf2bMMuFv0Cz4u5YZ/nlcd84G5O
I78lItSKMhGjYMS541p2VAoDgTQ8y9cEqow0rGNZy5T4npjl8IjlI1RdQ7J4/hwe
F5IGRpoiAxjSZawppiGC16Bp6IIKVEg4VyCGMg1pukJ0ExiQ6YoeAlimDKIQ9UN2
eWnUlsJQ2CT5/jpWfpR4ZQ6Xqu/d3Z4USAgGUpLH+OcVCI6d+OfoPnelBU7w9r6a
0m87ptOJHXPg5Tk/vIFCNJJWwyKLksuaU7qDvT+XTPp0TGtaWVdBc88cqRqaeutc
wcubaxaitOSjGXfOd63GXRvDz7NbyfD85ra8w18Gq5Yn762b++mQnZ01pbszULlt
8qmS09c2n+QlE6aFl7y2zy6oTa19lKrv2ba0pXFpd9GqngcViSsjGSOZ7v6q1VO2
7Cvdb13JG//0hpM+cBHMmNZzIKfx/e0Z62e9ff+4MNjc8/Xji9yWo/0PdtYOzXuz
vuEgfDf9TNL1jieDUr2zdsOldX2p3+tXzP7QsOzC4nhvQ0ZTvKnzKj7d8WzmE690
8JVWvGnc45y2qmXvLGVXx/AvPA8+DQ==
=680r
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/secdsm_ia

==================================================================